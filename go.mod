module git.openprivacy.ca/sarah/garden

go 1.13

require (
	cwtch.im/tapir v0.1.16
	git.openprivacy.ca/openprivacy/connectivity v1.1.1
	git.openprivacy.ca/openprivacy/log v1.0.0
	github.com/therecipe/qt v0.0.0-20191101232336-18864661ae4f
)
