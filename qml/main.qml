import QtQuick 2.7 //ApplicationWindow
import QtQuick.Controls 2.1 //Dialog
import QtQuick.Layouts 1.12
import QtQuick.Controls 2.12

import QtQuick 2.0


ApplicationWindow {
    id: root
    visible: true
    title: "Garden"
    minimumWidth: 640
    minimumHeight: 640



    Canvas {
        id: canvas
        width: parent.width
        height: parent.height

        property real lastX
        property real lastY
        property bool clear

        onPaint: {
            var ctx = getContext("2d");
            if (!clear) {
                ctx.fillStyle = Qt.rgba(0x8e/0xff, 0x64/0xff, 0xa5/0xff, 1);
                ctx.fillRect(0, 0, width, height);
                clear = true;
            }
            ctx.fillStyle = Qt.rgba(0xfd/0xff, 0xf3/0xff, 0xfc/0xff, 0.3);
            ctx.fillRect(canvas.lastX, canvas.lastY, 5, 5);
        }
        MouseArea {
            id: area
            anchors.fill: parent
            onPositionChanged: {
                canvas.lastX = mouseX
                canvas.lastY = mouseY
                canvas.requestPaint()
                garden.newDrawEvent(canvas.lastX,canvas.lastY )
            }
        }
    }

       Connections { // POPUPS ARE INVOKED BY GO FUNCS
                target: garden
                onDrawEvent: function(x,y) {
                    canvas.lastX = x
                    canvas.lastY = y
                    canvas.requestPaint()
                }
        }
}
