package main

import (
	"cwtch.im/tapir"
	tapirtor "cwtch.im/tapir/networks/tor"
	"cwtch.im/tapir/primitives"
	"fmt"
	"git.openprivacy.ca/openprivacy/connectivity"
	"git.openprivacy.ca/openprivacy/connectivity/tor"
	"git.openprivacy.ca/openprivacy/log"
	api "git.openprivacy.ca/sarah/garden/ui"
	"github.com/therecipe/qt/core"
	"github.com/therecipe/qt/qml"
	"github.com/therecipe/qt/quickcontrols2"
	"github.com/therecipe/qt/widgets"
	"os"
)

func main() {
	log.SetLevel(log.LevelDebug)
	var acn connectivity.ACN
	acn, _ = tor.NewTorACN(".", "")
	acn.WaitTillBootstrapped()

	core.QCoreApplication_SetAttribute(core.Qt__AA_EnableHighDpiScaling, true)

	app := widgets.NewQApplication(len(os.Args), os.Args)
	app.SetAttribute(core.Qt__AA_EnableHighDpiScaling, true)
	quickcontrols2.QQuickStyle_SetStyle("Material")
	engine := qml.NewQQmlApplicationEngine(nil)

	// Generate Server Keys
	id, sk := primitives.InitializeEphemeralIdentity()
	fmt.Printf("Ephemeral Garden: %v\n", id.Hostname())
	gapi := api.NewGardenAPI(nil)
	// Init the Server running the Simple App.
	var service tapir.Service
	service = new(tapirtor.BaseOnionService)
	service.Init(acn, sk, &id)

	if len(os.Args) < 2 {
		go service.Listen(gapi)
	} else {
		service.Connect(os.Args[1], gapi)
	}
	engine.RootContext().SetContextProperty("garden", gapi)
	// load the embedded qml file
	// created by either qtrcc or qtdeploy
	//engine.Load(core.NewQUrl3("qrc:/qml/main.qml", 0))
	// you can also load a local file like this instead:
	engine.Load(core.QUrl_FromLocalFile("./qml/main.qml"))

	// start the main Qt event loop
	// and block until app.Exit() is called
	// or the window is closed by the user
	widgets.QApplication_Exec()
}
