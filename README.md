# Ephemeral Garden

![](./garden.png)

## To Build

You will need to have installed Qt development libs (e.g. 5.13.1) and  [therecipe/qt](https://github.com/therecipe/qt). 

    go mod vendor
    qtdeploy  --qt_version "5.13.1" build linux
    
## To Host a Garden
       ./deploy/linux/garden
       Ephemeral Garden: hctctpqdjn7n6bgugg7u4hq2adbbeapbg3qt7m2jy42pfizbhcmty5ad
       
Wait for the other user to connect.       

## To Connect to a Garden
        ./deploy/linux/garden hctctpqdjn7n6bgugg7u4hq2adbbeapbg3qt7m2jy42pfizbhcmty5ad
        
Currently only 1 person can connect to garden at a time.